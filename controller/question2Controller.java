package nl.inholland.JoepMes.controller;



        import nl.inholland.JoepMes.service.ExecutorService;
        import org.springframework.http.MediaType;
        import org.springframework.web.bind.annotation.*;

        import java.util.List;

        import java.awt.*;

@RestController
@RequestMapping("/qeustion2")
public class question2Controller {

    private ExecutorService service;

    public question2Controller(ExecutorService service) {
        this.service = service;
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Integer> getListofInts() {
        return service.getIntList();
    }


}
