package nl.inholland.JoepMes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.logging.Level;
import java.util.logging.Logger;

@SpringBootApplication
public class JoepMesApplication {
	private static final Logger logger = Logger.getLogger(JoepMesApplication.class.getName());

	public static void main(String[] args) {
		logger.log(Level.INFO, "-- Exam Question 1 completed --");

		SpringApplication.run(JoepMesApplication.class, args);
	}

}
