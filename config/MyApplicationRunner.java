package nl.inholland.JoepMes.config;

import nl.inholland.JoepMes.JoepMesApplication;
import nl.inholland.JoepMes.model.Address;
import nl.inholland.JoepMes.repositiry.AddressRepositiry;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class MyApplicationRunner implements ApplicationRunner {

    private static final Logger logger = Logger.getLogger(JoepMesApplication.class.getName());

    private AddressRepositiry addressRepositiry;

    public MyApplicationRunner(AddressRepositiry addressRepositiry) {
        this.addressRepositiry = addressRepositiry;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        Files.lines(Paths.get("src/main/resources/address.csv"))
                .forEach(
                        line -> addressRepositiry.save(
                                new Address(line.split(",")[0],
                                        line.split(",")[1],
                                        line.split(",")[2],
                                        line.split(",")[3]))
                );

        addressRepositiry.findAll()
                .forEach(System.out::println);


        logger.log(Level.INFO, "-- Exam Question 3 completed --");
    }
}
