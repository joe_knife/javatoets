package nl.inholland.JoepMes.service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class ExecutorService {
    private int counter = 0;
    List<Integer> ints = new ArrayList<Integer>(
            Arrays.asList(
                    1,
                    2,
                    3
            )
    );

    private void incrementAndPrint() {
        counter++;
        System.out.println(Thread.currentThread().getName() + ", " + counter);
    }

    public List<Integer> getIntList() {


        return ints;
    }

}
